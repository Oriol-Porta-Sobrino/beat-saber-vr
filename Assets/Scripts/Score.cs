using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{

    private int score;

    [SerializeField]
    private TextMeshProUGUI textoMeshPro;
    [SerializeField]
    private AudioSource correctSound;
    [SerializeField]
    private AudioSource incorrectSound;

    void Start()
    {
        CubeScript.correctCubeDestroyed += increaseScore;
        CubeScript.incorrectCubeDestroyed += decreaseScore;
    }

    public void increaseScore()
    {
        correctSound.Play();
        score += 10;
        printScore();
    }

    public void decreaseScore()
    {
        incorrectSound.Play();
        if (score > 0) {
            score -= 10;
            printScore();
        }
    }

    public void printScore()
    {
        textoMeshPro.text = "Score \n" + score;
    }
}
