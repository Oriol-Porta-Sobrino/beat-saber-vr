using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{

    [SerializeField]
    private GameObject[] cubes;
    [SerializeField] 
    private float beat;
    [SerializeField]
    private Transform[] spawnerPositions;

    private float timer;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > beat)
        {
            timer -= beat;
            GameObject cubeToSpawn = Instantiate(cubes[Random.Range(0, cubes.Length)], spawnerPositions[Random.Range(0, spawnerPositions.Length)]);
            //cubeToSpawn.transform.localPosition = Vector3.zero;
            cubeToSpawn.transform.Rotate(transform.forward, 90 * Random.Range(0, 4));
        }
        timer += Time.deltaTime;
    }
}
