using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CubeScript : MonoBehaviour
{

    [SerializeField]
    private GameObject appropiateSword;

    [SerializeField]
    private GameObject slicedCube;

    public static Action correctCubeDestroyed;
    public static Action incorrectCubeDestroyed;

    private int colisions = 0;


    [SerializeField]
    private float cubeSpeed = 0.8f;

    // Update is called once per frame
    void Update()
    {
        Vector3 vector3 = new Vector3(transform.position.x, transform.position.y, transform.position.z + cubeSpeed * Time.deltaTime);
        transform.position = vector3;
        if (transform.position.z > 16)
        {
            incorrectCubeDestroyed?.Invoke();
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (colisions == 0 && (other.gameObject.layer == 9 || other.gameObject.layer == 8)) { 
            colisions++;
            Rigidbody rigidbody = other.gameObject.GetComponentInParent<Rigidbody>();
            Vector3 velocity = rigidbody.velocity.normalized;
            bool goodDirection = checkCorrectDirection(velocity);
            if (goodDirection)
            {
                Debug.Log("Rotacion cubo: " + gameObject.transform.eulerAngles.z + " Velocity: " + velocity);
            }
            bool goodColor = checkCorrectColor(other);
            if (goodColor && goodDirection)
            {
                correctCubeDestroyed?.Invoke();
            } else
            {
                incorrectCubeDestroyed?.Invoke();
            }
            spawnSlicedCube();
            Destroy(gameObject);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        colisions--;
    }

    private void spawnSlicedCube()
    {
        GameObject slicedCubeSpawned = Instantiate(slicedCube, transform.position, Quaternion.identity);
        slicedCubeSpawned.transform.rotation = transform.rotation;
        //Para darle velocidad deberia hacerlo en cada hijo que es una parte diferente y a cada componente de ellos
        //Rigidbody slicedCubeRigidBody = slicedCubeSpawned.GetComponent<Rigidbody>();
        //slicedCubeRigidBody.velocity = new Vector3(transform.position.x, transform.position.y, transform.position.z + cubeSpeed * Time.deltaTime);
        //Debug.Log("Velocidad de la luna " + slicedCubeRigidBody);
    }

    private bool checkCorrectColor(Collider other)
    {
        if (this.gameObject.layer == 6 && other.gameObject.layer == 9)
        {
            return true;
        }
        else if (this.gameObject.layer == 7 && other.gameObject.layer == 8)
        {
            return true;
        }
        return false;
    }

    private bool checkCorrectDirection(Vector3 bladeVelocity)
    {
        if (Mathf.Abs(bladeVelocity.x) > Mathf.Abs(bladeVelocity.y))
        {
            if (bladeVelocity.x < 0 && this.gameObject.transform.rotation == new Quaternion(0.00000f, 0.00000f, 0.70711f, -0.70711f))
            {
                return true;
            }
            else if (bladeVelocity.x > 0 && this.gameObject.transform.rotation == new Quaternion(0.00000f, 0.00000f, 0.70711f, 0.70711f))
            {
                return true;
            }
        }
        else
        {
            if (bladeVelocity.y > 0 && this.gameObject.transform.rotation == new Quaternion(0.00000f, 0.00000f, 1.00000f, 0.00000f))
            {
                return true;
            }

            else if (bladeVelocity.y < 0 && this.gameObject.transform.rotation == new Quaternion(0.00000f, 0.00000f, 0.00000f, 1.00000f))
            {
                return true;
            }
        }
        return false;
    }

}
