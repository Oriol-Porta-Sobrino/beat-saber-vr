using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSlicedScript : MonoBehaviour
{

    [SerializeField]
    public float targetTime = 3.0f;

    [SerializeField]
    private float cubeSpeedMovement = 0.8f;

    [SerializeField]
    private float cubeSpeedRotation = 10.8f;

    private GameObject leftHalf;
    private GameObject rightHalf;

    private void Start()
    {
        leftHalf = transform.Find("sliced_cube/Cube").gameObject;
        rightHalf = transform.Find("sliced_cube/Cube.001").gameObject;
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 movementPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + cubeSpeedMovement * Time.deltaTime);
        gameObject.transform.position = movementPosition;
        //rightHalf.transform.Rotate(new Vector3(0, 0, cubeSpeedRotation) * Time.deltaTime);
        //leftHalf.transform.Rotate(new Vector3(0, 0, -cubeSpeedRotation) * Time.deltaTime);
        targetTime -= Time.deltaTime;
        if (targetTime <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
